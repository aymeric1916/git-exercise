# Évaluation Git

- Prénom et nom : Aymeric Prévost
- Commentaires : RAS

## Exercices

- 1 point - Fourcher et cloner le dépôt ##Fait##

- 1 point - Remplir votre nom ci-dessus ##Fait##

- 1 point - Reprendre dans master le commit de la branche `equality_refinement` via `cherry-pick` ##Fait##

- 1 point - Défaire le commit `remove unused subtract functions` ##Fait##

- 1 point - Fusionner dans `master` la branche `doc/intro` ##Fait##

- 2 points - Fusionner dans `master` la branche `add_distance` ##Fait##

- 1 point - Pousser le nouveau `master` ##Fait##

- 2 points - Créer une branche sur le dernier `master`, créer une nouvelle fonction de calcul, la committer, et pousser cette nouvelle branche ##Fait##

- 2 points - Rebaser la branche `add_tests` sur `master`

- 1 point - Pousser la branche `add_tests` sur le dépôt distant

- 2 points - Ouvrir une merge request à partir de cette branche `add_tests` sur le dépôt source


## Questions

- Donner l’auteur du commit qui a introduit la fonction `add` dans ce projet : L'auteur de la fonction add est "Père Noël"

- Indiquer quelle commande utiliser, dans un projet Git,  pour changer le message du dernier commit. Pour changer le message du dernier commit c'est : git commit --amend -m "Nouveau message"

- Quelle commande, inverse de `git add`, permet de retirer un fichier du stage (prochain commit) ? L'inverse de "git add" est "git reset"


- Donner le nom du créateur de Git : L'inventeur de ce dépôt git est Jalil Arfaoui et l'inventeur de git tout cours est Linus Torvalds

- Indiquer la(les) différence(s) entre les commandes `git init` et `git clone` : git init initialise un nouveau dépôt et git clone copie un dossier existant en local